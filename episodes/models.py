from django.db import models
from series_items.models import SeriesItems


class Episode(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    slug = models.SlugField(null=True, blank=True)
    draft = models.BooleanField(default=False)

    series_item = models.ForeignKey(SeriesItems, on_delete=models.CASCADE, related_name='episodes', null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
