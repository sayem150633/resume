from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import EpisodeViewSet

router = DefaultRouter()
router.register('episodes', EpisodeViewSet, basename='episodes')

app_name = "episodes"

urlpatterns = [
    path('', include(router.urls))
]
