from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .serializers import EpisodeSerializer
from .models import Episode
from post_comments.models import PostComment
from reactions.permissions import UpdateOwnItem


class EpisodeViewSet(ModelViewSet):
    serializer_class = EpisodeSerializer
    permission_classes = [AllowAny, ]
    queryset = Episode.objects.all()

    def list(self, request, *args, **kwargs):
        query_set = self.queryset.select_related('series_item', 'series_item__user').all()
        serializer = self.serializer_class(query_set, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


