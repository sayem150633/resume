from django.db.models import Avg

from rest_framework import serializers

from .models import Episode
from post_comments.serializers import ParentCommentSerializer
from rating.serializers import RatingSerializer
from users.serializers import UserSerializer


class EpisodeSerializer(serializers.ModelSerializer):
    from series_items.serializers import SeriesItemsSerializers
    series_item_details = SeriesItemsSerializers(source='series_item', read_only=True)
    # user_details = UserSerializer(source='series_item__user', read_only=True)

    class Meta:
        model = Episode
        fields = ['id', 'title', 'body', 'slug', 'draft', 'created_at', 'updated_at',
                  'series_item', 'series_item_details']
        read_only_fields = ['id', 'created_at', 'updated_at']


class EpisodeDetailsSeries(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField()

    class Meta:
        model = Episode
        fields = ['id', 'title', 'body', 'slug', 'draft', 'created_at', 'updated_at',
                  'rating']
        read_only_fields = ['id', 'created_at', 'updated_at']

    @staticmethod
    def get_rating(obj):
        return obj.rating_episode.all().aggregate(Avg('rating'))['rating__avg']
