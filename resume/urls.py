from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

admin.autodiscover()
admin.site.enable_nav_sidebar = False

# schema_view = get_swagger_view(title="Resume")

schema_view = get_schema_view(
    openapi.Info(
        title="Resume",
        default_version='v1',
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('api-auth/', include('rest_framework.urls')),
                  path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
                  path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
                  path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
                  path("api/users/", include("users.urls", namespace="users")),
                  path("api/", include("series_items.urls", namespace="series_items")),
                  path("api/", include("meta_data.urls", namespace="meta_data")),
                  path("api/", include("rating.urls", namespace="rating")),
                  path("api/", include("profiles.urls", namespace="profiles")),
                  path("api/", include("reactions.urls", namespace="reactions")),
                  path("api/", include("post_comments.urls", namespace="post_comments")),
                  path("api/", include("categories.urls", namespace="categories")),
                  path('api/', include('episodes.urls', namespace='episodes')),
                  path("api/", include("follow.urls", namespace='follow')),
                  path("api/", include("questionnaries.urls", namespace='questionnaries')),
                  # path('swagger-docs/', schema_view),
                  path('^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0),
                       name='schema-json'),
                  path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
                  path('^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += path('__debug__/', include(debug_toolbar.urls)),
