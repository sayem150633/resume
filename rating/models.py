from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from episodes.models import Episode

# episode relation add korte hbe
User = get_user_model()


class Rating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="rating_user")
    episode = models.ForeignKey(Episode, on_delete=models.CASCADE, related_name='rating_episode', blank=True, null=True)
    rating = models.IntegerField(validators=[MaxValueValidator(5), MinValueValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Rating: {}, User: {}'.format(self.rating, self.user.id)
