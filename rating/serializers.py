from rest_framework import serializers
from .models import Rating


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        # episode add
        fields = ['id', 'user', 'rating']
        read_only_fields = ['id']
        # read_only_fields = ['id', 'user']
