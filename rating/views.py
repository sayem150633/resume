from django.db.models.query_utils import Q
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin
from .permissions import UpdateOwnItem
from .models import Rating
from .serializers import RatingSerializer


class RatingView(GenericViewSet, CreateModelMixin):
    permission_classes = [IsAuthenticated, UpdateOwnItem]
    serializer_class = RatingSerializer
    queryset = Rating.objects.all()

    # def create(self, request, *args, **kwargs):
    #     data = request.data
    #     user = request.user
    #
    #     qs = self.queryset.filter(Q(episode=data['episode'], user=user))
    #     rating_data = {
    #         'episode': data['episode'],
    #         'user': user.id,
    #         'rating': data['rating']
    #     }
    #     if qs:
    #         serializer = self.serializer_class(instance=qs.first(), data=rating_data)
    #         if serializer.is_valid():
    #             serializer.save()
    #             return Response(serializer.data, status=status.HTTP_201_CREATED)
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    #
    #     else:
    #         serializer = self.serializer_class(data=rating_data)
    #         if serializer.is_valid():
    #             serializer.save(user=user)
    #             return Response(serializer.data, status=status.HTTP_201_CREATED)
    #         print("in if else", serializer.errors)
    #         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
