from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import RatingView

router = DefaultRouter()
router.register('rating', RatingView, basename='rating')
app_name = 'rating'

urlpatterns = [
    path("", include(router.urls))
]
