from django.db import models
from django.contrib.auth import get_user_model
# from ..post_comments.model import PostComment

User = get_user_model()


class Reaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reaction_user")
    reactions = [
        ('LIKE', 'like'),
        ('UNLIKE', 'Unlike')
    ]
    type = models.CharField(choices=reactions, max_length=10, null=True, blank=True)
    # post_comment = models.ForeignKey(PostComment, on_delete=models.CASCADE, related_name='reaction_post_comment')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Reaction: {}, User: {}'.format(self.type, self.user.id)