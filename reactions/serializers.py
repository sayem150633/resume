from rest_framework import serializers
from .models import Reaction
from users.serializers import UserSerializer


class ReactionSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Reaction
        fields = ['id', 'user', 'type']


class ReactionSerializerPost(serializers.ModelSerializer):
    class Meta:
        model = Reaction
        fields = ['id', 'user', 'type']
        # , 'post_comment'
