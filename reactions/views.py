from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin, CreateModelMixin
from .permissions import UpdateOwnItem
from .models import Reaction
from .serializers import ReactionSerializerPost


class ReactionViewSet(GenericViewSet, CreateModelMixin, UpdateOwnItem, DestroyModelMixin):
    permission_classes = [IsAuthenticated, UpdateOwnItem]
    serializer_class = ReactionSerializerPost
    queryset = Reaction.objects.all()
