from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ReactionViewSet

router = DefaultRouter()
router.register('reactions', ReactionViewSet, basename='reactions')

app_name = "reactions"

urlpatterns = [
    path("", include(router.urls)),
]