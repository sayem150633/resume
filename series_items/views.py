from django.shortcuts import render
from .models import SeriesItems
from .serializers import SeriesItemsSerializers
from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from users.permissions import UpdateOwnItem
from django.db.models.query_utils import Q

your_media_root = settings.MEDIA_ROOT

valid_file_type = ['jpg', 'jpeg', 'png', 'gif', 'webm']


# def upload_series_thumbnail(file_object):
#     file_name = file_object.name
#     file_type = file_name.split('.')[-1]
#     if file_type not in valid_file_type:
#         return False
#     url =

class SeriesItemViewSet(viewsets.ModelViewSet):
    serializer_class = SeriesItemsSerializers
    permission_classes = [AllowAny, ]
    queryset = SeriesItems.objects.all()

    def get_permissions(self):
        if self.action in ['list', 'retrieve', 'my_series']:
            self.permission_classes = [AllowAny]
        else:
            self.permission_classes = [IsAuthenticated,]
        return super(SeriesItemViewSet, self).get_permissions()

    def list(self, request, *args, **kwargs):
        qs_set = request.query_params
        qs = SeriesItems.objects.all()
        if 'categories' in qs_set:
            qs = SeriesItems.objects.filter(categories=qs_set.get('categories'))
        elif 'title' in qs_set:
            qs = SeriesItems.objects.filter(Q(title__icontains=qs_set['title']) |
                                            Q(user__username__icontains=qs_set.get('title'))).distinct()
        serializer = self.serializer_class(qs, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        user = request.user
        print(request.user)
        data = request.data
        categories = data.getlist('categories')
        print(type(categories))
        file_object = data.get('image', None)
        if file_object:
            name = file_object.name
            file_type = name.split('.')[-1]
            if file_type not in valid_file_type:
                return Response(
                    {"message": "Invalid File Type"},
                    status=status.HTTP_400_BAD_REQUEST
                )

        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            series = serializer.save(user=user, categories=categories)
            try:
                thumbnail_url = series.image.url
                series.thumbnail_url = thumbnail_url
            except:
                series.thumbnail_url = ''
            series.save()

            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs['pk']
        instance = SeriesItems.objects.filter(id=pk).first()
        if instance:
            serializer = SeriesItemsSerializers(instance)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response({"message": "Content does not exists."}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        pk = kwargs['pk']
        instance = SeriesItems.objects.get(id=pk)
        partial = kwargs.pop('partial', False)
        print(partial)

        data = request.data
        file_object = data.get('image', None)
        name = file_object.name
        file_type = name.split('.')[-1]
        if file_type not in valid_file_type:
            return Response(
                {"message": "Invalid File Type"},
                status=status.HTTP_400_BAD_REQUEST
            )

        serializer = self.serializer_class(instance, data=data)
        if serializer.is_valid():
            series = serializer.save()
            thumbnail_url = series.image.url
            series.thumbnail_url = thumbnail_url

            series.save()

            return Response(
                serializer.data,
                status=status.HTTP_201_CREATED
            )
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class GetOwnSeriesItems(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SeriesItemsSerializers
    queryset = SeriesItems.objects.all()

    def get(self, request, format=None):
        if request.user.is_authenticated:
            user = request.user
            qs = self.queryset.filter(user=user.id)
            serializer = self.serializer_class(qs, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({
            'details': 'Authentication credentials were not provided.'
        }, status=status.HTTP_401_UNAUTHORIZED)
