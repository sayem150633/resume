from django.apps import AppConfig


class SeriesItemsConfig(AppConfig):
    name = 'series_items'
