from  django.urls import reverse, resolve
from rest_framework.test import APITestCase
from ..views import SeriesItemViewSet


class TestUrl(APITestCase):
    def test_series_items_urls_resolves(self):
        url = reverse('series-items:series-items-list')
        self.assertEquals(resolve(url).func.__module__, SeriesItemViewSet.__module__)