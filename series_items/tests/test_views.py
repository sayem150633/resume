import os
import tempfile

from PIL import Image
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.conf import settings
from rest_framework.test import APITestCase, APIClient
from ..models import SeriesItems
from ..serializers import SeriesItemsSerializers
from rest_framework import status

BASE_URL = os.path.join(settings.ROOT_DIR, 'resume_souece/series_items/tests')


def make_test_data():
    payload = {
        'username': "test1",
        "email": "test@gmail.com",
        "password": "Ss@12324"
    }
    client = APIClient()
    user = create_user(**payload)
    url = reverse('series-items:series-items-list')
    # with tempfile.NamedTemporaryFile(suffix='.jpg') as ntf:
    #     img = Image.new('RGB', (10, 10))
    #     img.save(ntf, format='JPEG')
    #     ntf.seek(0)
    #     res = client.post(url, {'image': ntf}, format='multipart')

    with open(BASE_URL + '/soccer.jpg', 'rb') as fp:
        res = fp
        test_data = {
            'user': user,
            'title': "test title",
            'description': "test description",
            "redirect": "www.abc.com/123/",
            # "image": res,
            "thumbnail_author": "sayem",
            "slug": "test-title",
            "next_episode": "2020-12-18",
            "paused": True,
        }
        return SeriesItems.objects.create(**test_data)


def create_user(**params):
    user = get_user_model().objects.create_user(**params)
    return user


class TestSeriesItemsView(APITestCase):
    def setUp(self):
        self.client = APIClient()
        payload = {
            "username": "test",
            "email": "test_user@gmail.com",
            "password": "abc123ABC",
        }
        self.user = create_user(**payload)
        self.client.force_authenticate(user=self.user)

    def test_series_items_Get_list(self):
        make_test_data()
        res = self.client.get(reverse('series-items:series-items-list'))
        obj = SeriesItems.objects.all()
        serializer = SeriesItemsSerializers(obj, many=True)
        self.assertEquals(res.status_code, status.HTTP_200_OK)
        self.assertEquals(res.data, serializer.data)

    def test_series_items_GET_Retrieve(self):
        make_test_data()
        obj = SeriesItems.objects.all().first()
        serializer = SeriesItemsSerializers(obj)
        res = self.client.get(reverse('series-items:series-items-detail', kwargs={'pk': obj.id}))
        self.assertEquals(res.status_code, status.HTTP_200_OK)
        self.assertEquals(res.data, serializer.data)

