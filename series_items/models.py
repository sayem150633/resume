from django.db import models
from django.contrib.auth import get_user_model
from categories.models import Category

User = get_user_model()


class SeriesItems(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=255)
    description = models.TextField()
    redirect = models.URLField(max_length=255, null=True, blank=True)
    thumbnail_url = models.URLField()
    thumbnail_author = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField(upload_to="media", null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)
    next_episode = models.DateField()
    paused = models.BooleanField(default=False)

    categories = models.ManyToManyField(Category, related_name='series_categories')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
