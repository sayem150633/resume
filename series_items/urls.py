from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import SeriesItemViewSet,GetOwnSeriesItems

router = DefaultRouter()
router.register('series-items', SeriesItemViewSet, basename='series-items')

app_name = 'series-items'

urlpatterns = [
    path('', include(router.urls)),
    path('my-series', GetOwnSeriesItems.as_view(), name="my-series")
]
