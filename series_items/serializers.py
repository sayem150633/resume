from rest_framework import serializers
from .models import SeriesItems
from categories.serializers import CategorySerializer
from users.serializers import UserSerializer


class SeriesItemsSerializers(serializers.ModelSerializer):
    categories = CategorySerializer(read_only=True, many=True)

    class Meta:
        model = SeriesItems
        fields = ['id', 'user', 'title', 'description', 'redirect', "image", 'thumbnail_url', 'thumbnail_author',
                  'slug',
                  'next_episode', 'paused', 'categories', 'created_at', 'updated_at']
        read_only_fields = ['id', 'thumbnail_url', 'created_at', 'updated_at', 'user']


class SeriesItemsSerializerGET(serializers.ModelSerializer):
    from episodes.serializers import EpisodeDetailsSeries
    categories = CategorySerializer(read_only=True, many=True)
    episodes = EpisodeDetailsSeries(read_only=True, many=True)
    user = UserSerializer(read_only=True)

    class Meta:
        model = SeriesItems
        fields = ['id', 'user', 'title', 'description', 'redirect', "image", 'thumbnail_url', 'thumbnail_author',
                  'slug',
                  'next_episode', 'paused', 'categories', 'created_at', 'updated_at']
        read_only_fields = ['id', 'thumbnail_url', 'created_at', 'updated_at']
