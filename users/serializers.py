from rest_framework import serializers
from .models import User
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
import re


class UserRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'password', 'is_confirmed', 'is_blocked')
        extra_kwargs = {
            'password': {
                'write_only': True,
                'style': {
                    'input_type': 'password'
                }
            }
        }

    def validate_password(self, password):
        if len(password) < 8:
            raise serializers.ValidationError(
                _("This password is too short. It must contain at least 8 character."),
                code='password_too_short', )

        if not re.findall('\d', password):
            raise serializers.ValidationError(
                _("The password must contain at least 1 digit, 0-9."),
                code='password_no_number',
            )

        if not re.findall('[A-Z]', password):
            raise serializers.ValidationError(
                _("The password must contain at least 1 uppercase letter, A-Z."),
                code='password_no_upper',
            )

        if not re.findall('[a-z]', password):
            raise serializers.ValidationError(
                _("The password must contain at least 1 lowercase letter, a-z."),
                code='password_no_lower',
            )

        return password

    def create(self, validate_data):
        """Create a new user with hashed password"""
        return get_user_model().objects.create_user(**validate_data)

    def update(self, instance, validate_data):
        password = validate_data.pop('password', None)
        user = super().update(instance, validate_data)
        if password:
            user.set_password(password)
            user.save()
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'email']


class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.EmailField()


class ResetPasswordConfirmSerializer(serializers.Serializer):
    password = serializers.CharField()
