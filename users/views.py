import uuid
from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView, ListCreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.viewsets import GenericViewSet
from .serializers import UserRegistrationSerializer
from rest_framework.mixins import ListModelMixin
from .permissions import IsSuperUser, IsOwnerOrReadOnly
from rest_framework.response import Response
from rest_framework import status

User = get_user_model()


class CreateUserView(CreateAPIView):
    """Create a new user in the system"""
    permission_classes = [AllowAny, ]
    serializer_class = UserRegistrationSerializer


class UserViewSet(GenericViewSet, ListModelMixin):
    serializer_class = UserRegistrationSerializer
    queryset = get_user_model().objects.all()
    permission_classes = [IsAuthenticated, IsSuperUser]


# class Registration(ListCreateAPIView):
#     serializer_class = UserRegistrationSerializer
#     queryset = get_user_model().objects.all()
#     permission_classes = [IsOwnerOrReadOnly, ]
#
#     def post(self, request, *args, **kwargs):
#         user_serializer = UserRegistrationSerializer(data=request.data)
#         if user_serializer.is_valid():
#             user_serializer.save()
#             return Response(
#                 {
#                     'message': "Registration Successful",
#                     'data': {
#                         "user": user_serializer.data
#                     }
#                 },
#                 status=status.HTTP_201_CREATED
#             )
#         else:
#             return Response(
#                 {
#                     'message': "Registration Failed",
#                     'error': user_serializer.errors
#                 },
#                 status=status.HTTP_400_BAD_REQUEST
#             )
