from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.translation import gettext_lazy as _


class UserManager(BaseUserManager):
    def create_user(self, email, username, password, **kwargs):
        if not email:
            raise ValueError('Please Enter Your Email.')

        email = self.normalize_email(email)
        user = self.model(email=email, username=username, **kwargs)
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_superuser(self, email, username, password):
        user = self.create_user(email, username, password)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self.db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(_('username'), max_length=255, help_text=_('Required. 255 characters or fewer. '
                                                                           'Letters, digits and @/./+/-/_ only.'),
                                validators=[username_validator], unique=True)
    email = models.EmailField(_('email address'), unique=True)
    is_confirmed = models.BooleanField(default=True)
    is_blocked = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserManager()
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.email


class ResetPassword(models.Model):
    user_id = models.IntegerField()
    uuid = models.CharField(max_length=255)
    expire_time = models.DateTimeField()
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.uuid
