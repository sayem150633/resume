from rest_framework import permissions


class IsSuperUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user and request.user.is_superuser)


class UpdateOwnItem(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in [permissions.SAFE_METHODS]:
            return True
        return obj.user.id == request.user.id


class IsOwnerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return False
        return bool(obj.user.username == request.user.username)
