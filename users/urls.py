from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CreateUserView, UserViewSet
# from .views import Registration

router = DefaultRouter()
router.register('', UserViewSet, basename='users')

app_name = "users"

urlpatterns = [
    path("registration/", CreateUserView.as_view(), name="registration"),
    path("", include(router.urls)),
]
