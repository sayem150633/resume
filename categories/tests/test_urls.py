from django.urls import reverse, resolve
from rest_framework.test import APITestCase

from categories.views import CategoryViewSet


class TestUrls(APITestCase):
    def test_categories_urls_resolve_LIST(self):
        url = reverse('categories:categories-list')
        self.assertEquals(resolve(url).func.__module__, CategoryViewSet.__module__)

    def test_categories_urls_resolve_DETAILS(self):
        url = reverse('categories:categories-detail', kwargs={'pk': 1})
        self.assertEquals(resolve(url).func.__module__, CategoryViewSet.__module__)