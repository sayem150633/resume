from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import MetaData
from .serializers import MetaDataSerializer


class MetaDataView(APIView):
    permission_classes = [AllowAny, ]
    serializer_class = MetaDataSerializer
    query_set = MetaData.objects.all()

    def get(self, request):
        qs_str = request.query_params.getlist('key', None)
        if qs_str:
            qs = self.query_set.filter(key__in=qs_str)
        else:
            qs = self.query_set
        serializer = MetaDataSerializer(qs, many=True)
        return Response(serializer.data)
