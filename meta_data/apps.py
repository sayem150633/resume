from django.apps import AppConfig


class MetaDataConfig(AppConfig):
    name = 'meta_data'
