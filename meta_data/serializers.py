from rest_framework import serializers
from .models import MetaData


class MetaDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetaData
        fields = ['id', 'key', 'meta']
        read_only_fields = ['id']
