from django.db import models
from django.contrib.postgres.fields import JSONField


class MetaData(models.Model):
    key = models.CharField(max_length=255)
    meta = JSONField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.key
