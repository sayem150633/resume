from django.urls import path

from .views import MetaDataView

app_name = "meta_data"
urlpatterns = [
    path("meta-data/", MetaDataView.as_view(), name="meta-data"),
]