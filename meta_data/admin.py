from django.contrib import admin
from django.contrib.postgres.fields import JSONField

from django_json_widget.widgets import JSONEditorWidget

from .models import MetaData


@admin.register(MetaData)
class SiteMetaDataAdmin(admin.ModelAdmin):
    formfield_overrides = {
        JSONField: {'widget': JSONEditorWidget}
    }