from rest_framework import serializers
from .models import PostComment
# import sys
# sys.path.append("use")
from reactions.serializers import ReactionSerializer, ReactionSerializerPost
from users.serializers import UserSerializer


class CommentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    reactions = ReactionSerializer(source='reaction_post_comment', many=True, read_only=True)

    liked = serializers.SerializerMethodField()
    unliked = serializers.SerializerMethodField()
    reacted = serializers.SerializerMethodField()

    class Meta:
        model = PostComment
        fields = ['id', 'user', 'body', 'reactions', 'liked', 'unliked', 'reacted']

    @staticmethod
    def get_liked(obj):
        return obj.reaction_post_comment.filter(type="LIKE").count()

    @staticmethod
    def get_unliked(obj):
        return obj.reaction_post_comment.filter(type='UNLIKE').count()

    def get_reacted(self, obj):
        user = self.context['user'].id
        reactions = obj.reaction_post_comment.filter(user=user)

        if reactions:
            serializer = ReactionSerializerPOST(reactions.first())
            return serializer.data
        else:
            return False


class ParentCommentSerializer(serializers.ModelSerializer):
    reply_comments = CommentSerializer(source='comments', read_only=True, many=True)
    user = UserSerializer(read_only=True)
    reactions = ReactionSerializer(source='reaction_post_comment', many=True, read_only=True)

    liked = serializers.SerializerMethodField()
    unliked = serializers.SerializerMethodField()
    reacted = serializers.SerializerMethodField()

    class Meta:
        model = PostComment
        fields = ['id', 'user', 'body', 'reply_comments', 'reactions', 'liked', 'unliked', 'reacted']

    @staticmethod
    def get_liked(obj):
        return obj.reaction_post_comment.filter(type="LIKE").count()

    @staticmethod
    def get_unliked(obj):
        return obj.reaction_post_comment.filter(type='UNLIKE').count()

    def get_reacted(self, obj):
        user = self.context['user'].id
        reactions = obj.reaction_post_comment.filter(user=user)

        if reactions:
            serializer = ReactionSerializerPOST(reactions.first())
            return serializer.data
        else:
            return False


class PostCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostComment
        fields = ['id', 'user', 'body', 'parent_comment']
        # 'episode',
