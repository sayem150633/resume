from django.apps import AppConfig


class PostCommentsConfig(AppConfig):
    name = 'post_comments'
