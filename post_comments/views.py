from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin, CreateModelMixin

from .models import PostComment
from .serializers import PostCommentSerializer
from rating.permissions import UpdateOwnItem

User = get_user_model()


class PostCommentViewSet(GenericViewSet, CreateModelMixin, DestroyModelMixin, UpdateModelMixin):
    permission_classes = [IsAuthenticated, UpdateOwnItem]
    serializer_class = PostCommentSerializer
    queryset = PostComment.objects.all()

    def create(self, request, *args, **kwargs):
        data = request.data
        print(data['parent_comment'])
        if 'parent_comment' in data:
            print('Enter')
            print(data['parent_comment'])
            parent_comment = get_object_or_404(PostComment, id=data['parent_comment'])
            res = {
                'user': data['user'],
                'body': data['body'],
                'parent_comment': data['parent_comment'],
                # 'episode': parent_comment.episode.id
            }
            serializer = self.serializer_class(data=res)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = self.serializer_class(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)