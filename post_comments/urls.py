from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import PostCommentViewSet

router = DefaultRouter()
router.register('post-comments', PostCommentViewSet, basename='post-comments')

app_name = 'post_comments'

urlpatterns = [
    path("", include(router.urls)),
]