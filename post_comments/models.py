from django.db import models
from django.contrib.auth import get_user_model
# from ..episodes.models import Episode

User = get_user_model()


class PostComment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='post_comments_user')
    # episode = models.ForeignKey(Episode, on_delete=models.CASCADE, related_name='episode_post_comments')
    body = models.TextField()
    parent_comment = models.ForeignKey('self', related_name='comments', blank=True, null=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.body
