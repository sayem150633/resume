from django.db.models.query_utils import Q
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Follow
from .serializers import FollowSerializer


class HandleFollowUnfollowView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = FollowSerializer
    queryset = Follow.objects.all()

    def post(self, request):
        if request.user.is_authenticated:
            user = request.user
            following = request.data.get('following', None)
            qs = self.queryset.filter(Q(user=user.id, following=following)).first()
            print(qs)

            if qs:
                qs.delete()
                return Response({'message': 'Unfollow Successfully'}, status=status.HTTP_204_NO_CONTENT)

            else:
                follow_data = {
                    'user': user.id,
                    'following': following,
                }
                serializer = self.serializer_class(data=follow_data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_200_OK)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response({'detail': 'Authentication credentials were not provided.'},
                        status=status.HTTP_401_UNAUTHORIZED)
