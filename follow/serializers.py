from rest_framework.serializers import ModelSerializer

from .models import Follow


class FollowSerializer(ModelSerializer):
    class Meta:
        model = Follow
        fields = ['id', 'user', 'following']