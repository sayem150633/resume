from django.urls import path
from .views import HandleFollowUnfollowView

app_name = 'follow'

urlpatterns = [
    path("follow-unfollow", HandleFollowUnfollowView.as_view(), name="follow-unfollow"),
]