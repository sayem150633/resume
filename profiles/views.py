from rest_framework import status, viewsets
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Profile
from .serializers import ProfileSerializer
from .permissions import UpdateOwnItem

valid_file_type = ['jpg', 'jpeg', 'png', 'gif', 'webm']


class ProfileViewSet(viewsets.GenericViewSet, UpdateModelMixin):
    permission_classes = [IsAuthenticated, UpdateOwnItem]
    serializer_class = ProfileSerializer

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        pk = kwargs['pk']
        instance = Profile.objects.get(id=pk)
        data = request.data
        profile_pic = data.get('profile_pic', None)
        pro_pic_name = profile_pic.name
        pro_pic_type = pro_pic_name.split('.')[-1]
        if pro_pic_type not in valid_file_type:
            return Response(
                {"message": "Invalid File Type"},
                status=status.HTTP_400_BAD_REQUEST
            )
        cover_pic = data.get('cover_pic', None)
        cover_pic_name = cover_pic.name
        cover_pic_type = cover_pic_name.split('.')[-1]
        if cover_pic_type not in valid_file_type:
            return Response(
                {'message': "Invalid file type"},
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = self.serializer_class(instance, data=request.data)
        profile_setup = True if data.get('profile_setup') == 'true' else False
        if serializer.is_valid():
            serializer.save(profile_setup=profile_setup)
            return Response(
                serializer.data,
                status=status.HTTP_200_OK
            )

        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

