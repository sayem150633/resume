from django.contrib import admin
from questionnaries.models import Question, Options


class OptionAdmin(admin.StackedInline):
    model = Options


class QuestionAdmin(admin.ModelAdmin):
    inlines = [OptionAdmin]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Options)
