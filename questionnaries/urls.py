from django.urls import path
from questionnaries.views import QuestionApiView, ResponseApiView

app_name = "questionnaries"

urlpatterns = [
    path('questions/', QuestionApiView.as_view(), name="questions"),
    path('response/', ResponseApiView.as_view(), name="response"),
]
