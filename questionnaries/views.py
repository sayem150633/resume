from django.shortcuts import render
from rest_framework.views import APIView
from questionnaries.models import Question, Response
from questionnaries.serializers import QuestionSerializer, ResponseSerializer
from rest_framework.response import Response
from rest_framework import status


class QuestionApiView(APIView):
    queryset = Question
    serializer = QuestionSerializer

    def get(self, request, *args, **kwargs):
        question = self.queryset.objects.filter(is_active=True)
        serializer = self.serializer(question, many=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ResponseApiView(APIView):
    queryset = Response
    serializer = ResponseSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer(data=data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
